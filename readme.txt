# Get Started #

$ mkdir proj
$ git clone https://davidjbarnes@bitbucket.org/davidjbarnes/form-configuration-survey-designer-with-nodejs-and-angularjs.git proj
$ cd proj
$ mongod
$ nodemon server.js

Build your survey here: http://localhost:3000/builder
View your survey here: http://localhost:3000/survey

Check downloads section for screenshots.

**Do yourself a favor and:npm install -g nodemon**