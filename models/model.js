//Setup
var mongoose = require('mongoose');
var validators = require('mongoose-validators');
var Schema = mongoose.Schema, ObjectId = Schema.ObjectId;


var dbConnect = process.env.MONGOLAB_URI || 'mongodb://localhost/survey';

mongoose.connect(dbConnect, function (err, res) {
    if (err) {
        console.log ('ERROR connecting to: ' + dbConnect + '. ' + err);
    } else {
        console.log ('Succeeded connected to: ' + dbConnect);
    }
});


//Schemas and Validation
var QuestionSchema = new Schema({
    index: { type: Number, validate: validators.isNumeric({ message: "Invalid question index; must be numeric." }) },
    name: { type: String, validate: validators.isLength({ message: "Invalid question name; must be 2-20 characters in length." }, 2, 20) },
    text: { type: String, validate: validators.isLength({ message: "Invalid question text; must be 2-20 characters in length." }, 2, 20) },
    fieldType: { type: String, validate: validators.isLength({ message: "Invalid question fieldType; must be 2-20 characters in length." }, 2, 20) },
    answer: { type: String }
});

var PageSubmitSchema = new Schema({
    submitType: { type: String, validate: validators.isLength({ message: "Invalid submitType; must be 2-20 characters in length." }, 2, 20) },
    submitLabel: { type: String, validate: validators.isLength({ message: "Invalid submitLabel; must be 2-20 characters in length." }, 2, 20) }
});

var PageSchema = new Schema({
    index: { type: Number, validate: validators.isNumeric({ message: "Invalid page index; must be numeric." }) },
    name: { type: String, validate: validators.isLength({ message: "Invalid page name; must be 2-20 characters in length." }, 2, 20) },
    title: { type: String, validate: validators.isLength({ message: "Invalid page title; must be 2-20 characters in length." }, 2, 20) },
    submit : { type: PageSubmitSchema.tree },
    question: { type: [QuestionSchema] }
});

var SurveySchema = new Schema({
    appId: { type: Number, validate: validators.isNumeric({ message: "Invalid appId; must be numeric." }) },
    name: { type: String, validate: validators.isLength({ message: "Invalid survey name; must be 4-20 characters in length." }, 4, 20) },
    page: { type: [PageSchema] }
});

var ResponseSchema = new Schema({
    sessionId: { type: Number, validate: validators.isNumeric({ message: "Invalid sessionId; must be numeric." }) },
    appId: { type: Number, validate: validators.isNumeric({ message: "Invalid appId; must be numeric." }) },
    name: { type: String, validate: validators.isLength({ message: "Invalid survey name; must be 4-20 characters in length." }, 4, 20) },
    page: { type: [PageSchema] }
});

//Models
var PageSubmit = mongoose.model('PageSubmit', PageSubmitSchema);
var Question = mongoose.model('Question', QuestionSchema);
var Page = mongoose.model('Page', PageSchema);
var Survey = mongoose.model('Survey', SurveySchema);
var Response = mongoose.model('Response', ResponseSchema);

//Exports
module.exports.Response = mongoose.model('Response');
module.exports.Survey = mongoose.model('Survey');
module.exports.Page = mongoose.model('Page');
module.exports.Question = mongoose.model('Question');
module.exports.PageSubmit = mongoose.model('PageSubmit');