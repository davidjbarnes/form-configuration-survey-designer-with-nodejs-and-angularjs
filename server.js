//Setup
var express = require("express");
var fs = require("fs");
var bodyParser = require('body-parser');

var app = express();

//Config
app.use('/public', express.static(__dirname + '/public'));
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

//Database/Models
var Model = require('./models/model');
var Config = Model.Config;
var AnswerSet = Model.AnswerSet;

//Controllers/Routes
require('./controllers/apiController')(app, fs, __dirname);
require('./controllers/builderController')(app, fs, __dirname);
require('./controllers/dashboardController')(app, fs, __dirname);
require('./controllers/homeController')(app, fs, __dirname);
require('./controllers/surveyController')(app, fs, __dirname);

require("angoose").init(app, {
   'module-dirs':'./models',
   'mongo-opts': 'localhost:27017/survey',
});

//Go
app.listen(process.env.PORT || 3000);
console.log('Server running...');