//Survey Routes

module.exports = function(app, fs, AppDir){
    app.get('/survey', function(req, res) {
        res.sendFile(AppDir + '/public/html/survey/index.html');
    });
}