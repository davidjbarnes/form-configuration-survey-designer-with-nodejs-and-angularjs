//Builder Routes

module.exports = function(app, fs, AppDir){
    app.get('/builder/page', function(req, res) {
        res.sendFile(AppDir + '/public/html/builder/page.html');
    });
    app.get('/builder/question', function(req, res) {
        res.sendFile(AppDir + '/public/html/builder/question.html');
    });
}