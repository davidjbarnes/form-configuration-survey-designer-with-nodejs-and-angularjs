//Dashboard Routes

module.exports = function(app, fs, AppDir){
    app.get('/dashboard', function(req, res) {
        res.sendFile(AppDir + '/public/html/dashboard/index.html');
    });
}